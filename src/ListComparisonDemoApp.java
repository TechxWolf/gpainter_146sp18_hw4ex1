/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
*/
import java.util.*; // wildcard import all parts of the util class
/**
 * Application class TO demonstrates the performance of an ArrayList appear
 * that a LinkedList to be congruent with theory
 * @author gpainter@email.uscb.edu
 * @version 146sp18_hw4ex1
 */
public class ListComparisonDemoApp {
    
    /**
    *Main method for app
    * @param args
    */
    public static void main ( String []args )
    {
        String [] myStrings = { "zero", "one", "three", "four" };
        
        System.out.println("Original array of strings " + 
                Arrays.toString( myStrings)+ "\n");
        
        // Create two list objects ( one ArrayList, one LinkedList)  that
        // both contain the same data
        List<String> myArrayList = new ArrayList<>(Arrays.asList(myStrings));
        List<String> myLinkedList = new LinkedList<>(Arrays.asList(myStrings));
        
        myArrayList.add(2, "two");
        
        myLinkedList.add(2, "two");
        
        // Use system.nanoTime{} to help compute the elapsed time
        // it takes to execute  a function of code
        

        long startTime = System.nanoTime();
        System.out.println(myArrayList);
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert elements in the middle" +
                " of an ArrayList " + elapsedTime);
        
        
        startTime = System.nanoTime();
        System.out.println(myLinkedList);
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert elements in the middle" +
                " of a LinnkedList " + elapsedTime);
             
        
        String middleElement = "";
        
        startTime = System.nanoTime();
        middleElement = myArrayList.get( 2 ); // retrieves element at index 2
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert elements in the middle" +
                " of an ArrayList " + elapsedTime);
        
        //System.out.println("Middle element of myArrayList: " + middleElement);
        
        startTime = System.nanoTime();
        middleElement = myLinkedList.get( 2 );
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
          System.out.println("Elapsed time to insert elements in the middle" +
                " of a LinnkedList " + elapsedTime);
        
        //System.out.println("Middle element of myArrayList: " + middleElement);
        
        /*
        In Summary:
        
        ArrayLists and LinkedLists are LOGICALLY similar,
        but their RUN-TIME PERFORMANCE is different
        
        
        ArrayLists:
        Faster ( connstant time, o{c} ) for random access
        Slower( linnear time, O{n} ) for innsertions/removals
        
        LinkedLists:
        Slower ( linear time O{c} ) for randoma acess ( looking up an element by its inndex)
        Faster ( constant time O{n} for insertions/removals
        ( strictly speaking, constant time applies when inseting/removing
            from the head or tail, sinnce the additional steps are required
            for looking up the element at the some index in the middke, which
            does take linear time or has O{n} complexity)
        
        */
        
    }//end class main
    
}//end class ListComparisonDemoApp 
